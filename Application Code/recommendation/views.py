from django.shortcuts import render
from recommendation.models import Customer,Feedback
from recommendation.utils import hello,office,hardware,model
from django.shortcuts import get_object_or_404
# Create your views here.
class Recommendation():
	anti=[]


def home(request):
	return render(request,'home.html',{})

def customer(request):
	obj=None
	result=None
	result1=None
	result2=None
	value='0'
	if request.GET.get('ref_id'):
		value='1'
		result=Recommendation()
		result1=Recommendation()
		result2=Recommendation()
		ref_id=request.GET.get('ref_id')
		request.session['abc']=ref_id
		obj=get_object_or_404(Customer,customer_id=ref_id)
		result.anti=hello(obj)
		result1.anti=office(obj)
		result2.anti=hardware(obj)
	context={
		'object':obj,
		'value':value,
		'result':result,
		'result1':result1,
		'result2':result2,
	}
	return render(request,'customer.html',context)

def administrator(request):
	change={
		'value':'0'
	}
	if request.GET.get('username')!=None:
		username=request.GET.get('username')
		password=request.GET.get('password')
		if(username=='test' ):
			if(password=='test1234'):
				change['value']='1'
				return render(request,'admin_login.html',{})
			else:
				change['value']='2'
		else:
			change['value']='2'

	print(change['value'])		
	return render(request,'admin.html',change)

def admin_login(request):
	return render(request,'admin_login.html',{})

def contact(request):
	return render(request,'contact.html',{})

def CustomerDetailView(request,my_id):
	customer=Customer.objects.get(id=my_id)
	context={
	'customer':customer
	}
	return render(request,'customer_detail.html',context)

def about(request):
	return render(request,'about.html',{})

def feedback(request):
	obj=None
	result=None
	ref_id=request.session.get('abc')

	if (ref_id!='0'):
		result=Recommendation()
		obj=get_object_or_404(Customer,customer_id=ref_id)
		result.anti=model(obj)
	context={
		'object':obj,
		'result':result
	}
	#f=open("comment.txt","a+")
	#Customer.objects.create(customer_id=4,antivirus_version=5,antivirus_expiry_date="2020-03-21",office_expiry_date="2020-03-21",in_home_hardware_service_expiry_date="2020-03-21",warranty_expiry_date="2020-03-21",office_data="Home and Student",sys_model="Home",purchase_data="2020-03-21")
	if (request.GET.get('comment')):
		print(request.GET.get('comment'))
		sentence=request.GET.get('comment')
		Feedback.objects.create(customer_id=ref_id,feedback=sentence)
		#f.write(sentence)
	return render(request,'feedback.html',context)

def buy(request):
	return render(request,'buy.html',{})

def buy1(request):
	return render(request,'buy1.html',{})

def buy2(request):
	return render(request,'buy2.html',{})

def buy3(request):
	return render(request,'buy3.html',{})
