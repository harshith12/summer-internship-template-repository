from django.urls import path
from . import views
from django.conf.urls import url

urlpatterns= [
	path('',views.home,name='home'),
    path('customer/',views.customer,name='customer'),
    path('administrator/',views.administrator,name='administrator'),
    path('contact/',views.contact,name='contact'),
    path('admin_login/',views.admin_login,name='admin_login'),
    path('about/',views.about,name='about'),
    path('customer/<int:my_id>',views.CustomerDetailView,name='customer-detail'),
    path('feedback/',views.feedback,name='feedback'),
    path('buy/',views.buy,name='buy'),
    path('buy1/',views.buy1,name='buy1'),
    path('buy2/',views.buy2,name='buy2'),
    path('buy3/',views.buy3,name='buy3')
]